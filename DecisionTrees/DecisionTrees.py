import NodeObj
from csv import reader
from random import seed
from random import randrange

def gini_index(groups, classes):
    n = float(sum([len(group) for group in groups]))
    gini = 0

    for group in groups:
        g_size = float(len(group))
        if g_size == 0:
            continue

        p_sum = 0
        for class_val in classes:
            c = float([row[-1] for row in group].count(class_val))
            p_sum += (c / g_size) ** 2
        gini += (g_size / n) * (1 - p_sum)

    return gini


def test_split(index, value, dataset):
    left, right = list(), list()
    for row in dataset:
        if row[index] < value:
            left.append(row)
        else:
            right.append(row)
    return left, right


def get_split(dataset):
    classes = list(set(row[-1] for row in dataset))
    b_index, b_value, b_score, b_groups = 999, 999, 999, None
    for index in range(len(dataset[0]) - 1):
        for row in dataset:
            groups = test_split(index, row[index], dataset)
            gini = gini_index(groups, classes)
            if gini < b_score:
                b_index, b_value, b_score, b_groups = index, row[index], gini, groups
    return {'index': b_index, 'value': b_value, 'groups': b_groups}


def build_tree(dataset, max_depth, min_nodes, current_depth):
    data_split = get_split(dataset)
    groups = data_split['groups']
    value = data_split['value']
    index = data_split['index']
    root = NodeObj.Node(value, index)


    if current_depth >= max_depth or len(dataset) <= min_nodes:
        for group in groups:
            if len(group) == 0:
                continue
            node = NodeObj.Node(-1, -1)
            node.make_it_terminal()
            node.rows_array = group.copy()

            root.children.append(node)
        return root

    for group in groups:
        if len(group) == 0:
            continue
        n = build_tree(group, max_depth, min_nodes, current_depth + 1)
        root.children.append(n)

    return root


def predict(node, row):
    if node.is_terminal:
        return node.decide()

    index = node.index
    if row[index] < node.value:
        return predict(node.children[0], row)
    else:
        # mlahav selotejp fix, ker ce je podatke razbiu na 2 dela in je en prazn, lah da ma sam en node,
        # za tega pa neves a je levo al je desno, ker tega nikjer ne belezis.
        if len(node.children) == 1:
            return predict(node.children[0], row)
        return predict(node.children[1], row)



# Load a CSV file
def load_csv(filename):
    file = open(filename, "rt")
    lines = reader(file)
    dataset = list(lines)
    return dataset

# Convert string column to float
def str_column_to_float(dataset, column):
    for row in dataset:
        row[column] = float(row[column].strip())

# Split a dataset into k folds
def cross_validation_split(dataset, n_folds):
    dataset_split = list()
    dataset_copy = list(dataset)
    fold_size = int(len(dataset) / n_folds)
    for i in range(n_folds):
        fold = list()
        while len(fold) < fold_size:
            index = randrange(len(dataset_copy))
            fold.append(dataset_copy.pop(index))
        dataset_split.append(fold)
    return dataset_split

# Calculate accuracy percentage
def accuracy_metric(actual, predicted):
    correct = 0
    for i in range(len(actual)):
        if actual[i] == predicted[i]:
            correct += 1
    return correct / float(len(actual)) * 100.0

# Evaluate an algorithm using a cross validation split
def evaluate_algorithm(dataset, algorithm, n_folds, *args):
    folds = cross_validation_split(dataset, n_folds)
    scores = list()
    for fold in folds:
        train_set = list(folds)
        train_set.remove(fold)
        train_set = sum(train_set, [])
        test_set = list()
        for row in fold:
            row_copy = list(row)
            test_set.append(row_copy)
            row_copy[-1] = None
        predicted = algorithm(train_set, test_set, *args)
        actual = [row[-1] for row in fold]
        accuracy = accuracy_metric(actual, predicted)
        scores.append(accuracy)
    return scores

# Classification and Regression Tree Algorithm
def decision_tree(train, test, max_depth, min_size):
    tree = build_tree(train, max_depth, min_size, 1)
    predictions = list()
    for row in test:
        prediction = predict(tree, row)
        predictions.append(prediction)
    return(predictions)

"""
dataset = [[2.771244718, 1.784783929, 0],
           [1.728571309, 1.169761413, 0],
           [3.678319846, 2.81281357, 0],
           [3.961043357, 2.61995032, 0],
           [2.999208922, 2.209014212, 0],
           [7.497545867, 3.162953546, 1],
           [9.00220326, 3.339047188, 1],
           [7.444542326, 0.476683375, 1],
           [10.12493903, 3.234550982, 1],
           [6.642287351, 3.319983761, 1]]
tree = build_tree(dataset, 3, 1, 1)

print(predict(tree, dataset[9]))
"""

seed(1)
filename = 'banknote.csv'
dataset = load_csv(filename)
for i in range(len(dataset[0])):
    str_column_to_float(dataset, i)
n_folds = 5
max_depth = 5
min_size = 10

scores = evaluate_algorithm(dataset, decision_tree, n_folds, max_depth, min_size)
print('Scores: %s' % scores)
print('Mean Accuracy: %.3f%%' % (sum(scores)/float(len(scores))))

#tree = build_tree(dataset, max_depth, min_size, 1)
print("done")
