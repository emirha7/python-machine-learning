class Node:
    def __init__(self, index):
        self.index = index

        self.children = dict()
        self.rows_array = []
        self.is_terminal = False

    def make_it_terminal(self):
        self.is_terminal = True

    def decide(self):
        a = [row[-1] for row in self.rows_array]
        return max(set(a), key=a.count)
