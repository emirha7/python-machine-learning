from NodeObj2 import Node
from csv import reader
from random import seed
from random import randrange

def load_csv(filename):
    file = open(filename, "rt")
    lines = reader(file)
    dataset = list(lines)
    return dataset

def gini_index(groups, classes):
    n = float(sum([len(group) for group in groups]))
    gini = 0

    for key in groups:
        g_size = float(len(groups[key]))
        if g_size == 0:
            continue

        p_sum = 0
        for class_val in classes:
            c = float([row[-1] for row in groups[key]].count(class_val))
            p_sum += (c / g_size) ** 2
        gini += (g_size / n) * (1 - p_sum)

    return gini

def feature_gini(dataset, index):
    feature_values = set([row[index] for row in dataset])
    classes = set(row[-1] for row in dataset)

    groups = dict()
    for key in feature_values:
        groups[key] = list()
        for row in dataset:
            if row[index] == key:
                groups[key].append(row)

    return {'gini': gini_index(groups, classes), 'groups': groups }


def split_data(dataset):
    b_gini = 999
    b_index = -1
    b_groups = None

    for index in range(len(dataset[0]) - 1):
        result = feature_gini(dataset, index)
        gini_value = result['gini']
        groups = result['groups']
        if gini_value < b_gini:
            b_gini = gini_value
            b_groups = groups
            b_index = index

    return {'groups': b_groups, 'index': b_index}

def build_tree(dataset, min_nodes, max_depth, current_depth):
    split_result = split_data(dataset)
    groups = split_result['groups']
    feature_index = split_result['index']

    root = Node(feature_index)

    if len(dataset) < min_nodes or current_depth >= max_depth:
        root.make_it_terminal()
        root.rows_array = dataset.copy()
        return root

    for key in groups:
        n = build_tree(groups[key], min_nodes, max_depth, current_depth+1)
        if not (key in root.children):
            root.children[key] = list()
        root.children[key].append(n)

    return root

def print_tree(node):
    if node.is_terminal:
        rows = [row[-1] for row in node.rows_array]
        print(rows)
        return

    print("feature = ", node.index)

    for key in node.children:
        for child in node.children[key]:
            print_tree(child)



def predict(tree, row):

    if tree.is_terminal:
        return tree.decide()

    feature = row[tree.index]
    for child in tree.children[feature]:
        return predict(child, row)


seed(1)
filename = 'mismas.csv'
dataset = load_csv(filename)

tree = build_tree(dataset, 1, 2, 0)
#print_tree(tree)
print(predict(tree, dataset[12]))