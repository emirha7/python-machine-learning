from math import sqrt
from math import exp
from math import pi
from csv import reader
from random import seed
from random import randrange

def seperateByClass(dataset):
    separated = dict()
    for i in range(len(dataset)):
        vector = dataset[i]
        classValue = vector[-1]
        if (classValue not in separated):
            separated[classValue] = list()
        separated[classValue].append(vector)
    return separated

def mean(numbers):
    return sum(numbers)/float(len(numbers))

def stdev(numbers):
    avg = mean(numbers)
    variance = sum([(x - avg)**2 for x in numbers]) / float(len(numbers) - 1)
    return sqrt(variance)

def summarizeDataSet(dataset):
    numberOfParams = len(dataset[0])-1
    list = []
    for i in range(numberOfParams):
        column = []
        for row in dataset:
            column.append(row[i])
        list.append((mean(column), stdev(column), len(column)))
    return list

def summarizeByClass(dataset):
    separated = seperateByClass(dataset) # za vsak class ma listo rowov k mu pripadajo
    results = dict()
    for className, rows in separated.items():
        results[className] = summarizeDataSet(rows)
    return results

def gauss(x, mean, stdev):
    exponent = exp(-((x-mean)**2 / (2 * stdev**2 )))
    return (1 / (sqrt(2 * pi) * stdev)) * exponent

def calculateClassProbabilites(summaries, row):
    numberOfRows = sum([summaries[key][0][2] for key in summaries])
    probs = dict()

    for classValue, classSummaries in summaries.items():
        p = classSummaries[0][2] / float(numberOfRows)
        for i in range(len(row)-1): #x1, x2
            mean, stdev, _ = classSummaries[i]
            p *= gauss(row[i], mean, stdev)
        probs[classValue] = p
    return probs

def loadCsv(filename):
    dataset = list()
    with open(filename, 'r') as file:
        csvReader = reader(file)
        for row in csvReader:
            if not row:
                continue
            dataset.append(row)
    return dataset

def strColumnToFloat(dataset, column):
    for row in dataset:
        row[column] = float(row[column].strip())

def str_column_to_int(dataset, column):
    class_values = [row[column] for row in dataset]
    unique = set(class_values)
    lookup = dict()
    for i, value in enumerate(unique):
        lookup[value] = i
    for row in dataset:
        row[column] = lookup[row[column]]
    return lookup

def crossValidationSplit(dataset, nFolds):
    folds = []
    foldLength = int(len(dataset) / nFolds)
    datasetTmp = list(dataset)
    for _ in range(0, nFolds):
        fold = []
        for _ in range(0, foldLength):
            randomIndex = randrange(len(datasetTmp))
            fold.append(datasetTmp.pop(randomIndex))
        folds.append(fold)
    return folds

def accuracyMetric(actual, predicted):
    hit = 0
    for i in range(len(actual)):
        if actual[i] == predicted[i]:
            hit += 1
    return hit / float(len(actual))

def predict(dataset, row):
    summaries = summarizeByClass(dataset)
    probs = calculateClassProbabilites(summaries, row)
    max = float('-inf')
    maxKey = None
    for key in probs:
        if probs[key] > max:
            max = probs[key]
            maxKey = key
    return maxKey


def evaluate(dataset, nFolds):
    folds = crossValidationSplit(dataset, nFolds)
    scores = list()
    for fold in folds:
        trainSet = list(folds)
        trainSet.remove(fold)
        trainSet = sum(trainSet, [])

        predictions = []
        actual = []

        for row in fold:
            p = predict(trainSet, row)
            predictions.append(p)
            actual.append(row[-1])

        scores.append(accuracyMetric(actual, predictions))
    return scores


dataset = [[3.393533211,2.331273381,0],
    [3.110073483,1.781539638,0],
    [1.343808831,3.368360954,0],
    [3.582294042,4.67917911,0],
    [2.280362439,2.866990263,0],
    [7.423436942,4.696522875,1],
    [5.745051997,3.533989803,1],
    [9.172168622,2.511101045,1],
    [7.792783481,3.424088941,1],
    [7.939820817,0.791637231,1]]

# Test Naive Bayes on Iris Dataset
seed(1)
filename = 'iris.csv'
dataset = loadCsv(filename)
for i in range(len(dataset[0])-1):
    strColumnToFloat(dataset, i)
# convert class column to integers
str_column_to_int(dataset, len(dataset[0])-1)

n_fold = 5
scores = evaluate(dataset, n_fold)
print('scores: %s', scores)
print('Mean accuracy ', mean(scores))