from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from sklearn.model_selection import cross_val_score
from sklearn.metrics import auc

def drawGraph(y_test, y_pred, predictWithProbs, header, clf, X_test):
    fpr, tpr, thr = roc_curve(y_test, predictWithProbs[:,1])
    auc = roc_auc_score(y_test, predictWithProbs[:,1])
    f1 = f1_score(y_test, y_pred)
    acc = accuracy_score(y_test, y_pred)
    scores = cross_val_score(clf, X_test, y_test, cv=10)
    print("mean = ", scores.mean(), " stdev = ", scores.std())
    print("AUC = ", auc, "accuracy = ", acc)
    print("F1 = ", f1)
    plt.plot(fpr, tpr, label=header)
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.legend((header))
    plt.show()

def analyzeSVM(y_pred, y_test, y_score, clf, X_test):
    acc = accuracy_score(y_test, y_pred)
    print("acc = ", acc)
    y_vals = [y for y in y_test]

    fpr, tpr, thr = roc_curve(y_vals, y_score)
    roc_auc = auc(fpr, tpr)
    print("AUC = ",roc_auc)
    print("F1: ", f1_score(y_test, y_pred))

    scores = cross_val_score(clf, X_test, y_test, cv=10)
    print("mean = ", scores.mean(), " stdev = ", scores.std())

    plt.figure()
    plt.plot(fpr, tpr, color='darkorange',
             lw=2, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()