from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import train_test_split
import Analyzer as analyzer

def train(x,y):
    print("===================== BAYES CLASSIFIER =====================")
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)
    gnb = GaussianNB()
    gnb = gnb.fit(X_train, y_train)

    y_pred = gnb.predict(X_test)
    y_pred_prob = gnb.predict_proba(X_test)
    analyzer.drawGraph(y_test, y_pred, y_pred_prob, "gauss", gnb, X_test)