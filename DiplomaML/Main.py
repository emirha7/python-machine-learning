import DataParser as dataParser
import RandomForest as rfc
import Bayes as bayes
import SVM as svm
import Dummy as dummy
import Ada as adaboost
import Tree as tree

def main():
    data = dataParser.parseData('data.csv')
    featureNames = ['user activity', 'env brightness', 'screen brightness',
                    'time of day', 'internet speed', 'battery level',
                    'images', 'theme', 'layout', 'font size']
    x = data[featureNames]
    y = data['output']

    dummy.train(x,y)
    rfc.train(x,y)
    bayes.train(x, y)
    svm.train(x,y)
    adaboost.train(x,y)
    tree.train(x,y)

main()