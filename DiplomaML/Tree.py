from sklearn import tree
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import f1_score
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_score

def train(x,y):
    print(" ================= TREE ======================")
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2)

    for i in range(1, 10):
        clf = tree.DecisionTreeClassifier(max_depth=i)
        clf.fit(X_train, y_train)

        y_pred = clf.predict(X_test)
        y_probs = clf.predict_proba(X_test)

        fpr, tpr, thresh = roc_curve(y_test, y_probs[:, 1])

        auc = roc_auc_score(y_test, y_probs[:, 1])
        print("F1: ", f1_score(y_test, y_pred))
        print("Depth =", str(i), "AUC = ", auc, "accuracy = ", accuracy_score(y_test, clf.predict(X_test)))
        scores = cross_val_score(clf, X_test, y_test, cv=10)
        print("mean = ", scores.mean(), " stdev = ", scores.std())

        plt.plot(fpr, tpr, label='p'+str(i))

    plt.legend(('p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8', 'p9', 'p10'))
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.show()
