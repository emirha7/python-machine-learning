import ValueMapper as valueMapper
import pandas as pd

def readFile(filename):
    with open(filename) as f:
        rows = f.readlines()

    rows = [x.strip() for x in rows]
    return rows

def parseData(filename):
    features = []
    labels = []
    rows = readFile(filename)

    for row in rows:
        splitedRow = row.split(";")
        pa = int(splitedRow[10])
        ra = int(splitedRow[11])
        ia = int(splitedRow[12])

        score = pa + ra + ia

        if score >= 3:
            labels.append(1)
        else:
            labels.append(0)

        userActivity = valueMapper.userActivityHM[splitedRow[0]]
        brightness = int(splitedRow[1])
        screenBrightness = int(splitedRow[2])
        timeOfDay = int(splitedRow[3])

        if splitedRow[4] == "4G" or splitedRow[4] == "3G" or splitedRow[4] == "2G":
            internetSpeed = valueMapper.internetHM[splitedRow[4]]
        else:
            internetSpeed = int(splitedRow[4])

        batteryLevel = int(splitedRow[5])
        showImages = valueMapper.imageViewHM[splitedRow[6]]
        theme = valueMapper.themeViewHM[splitedRow[7]]
        layout = valueMapper.layoutViewHM[splitedRow[8]]
        fontSize = valueMapper.fontSizeViewHM[splitedRow[9]]

        features.append([userActivity, brightness, screenBrightness, timeOfDay, internetSpeed,
                         batteryLevel, showImages, theme, layout, fontSize])

    return pd.DataFrame({
        'user activity': getColumn(features, 0),
        'env brightness': getColumn(features, 1),
        'screen brightness': getColumn(features, 2),
        'time of day': getColumn(features, 3),
        'internet speed': getColumn(features, 4),
        'battery level': getColumn(features, 5),
        'images': getColumn(features, 6),
        'theme': getColumn(features, 7),
        'layout': getColumn(features, 8),
        'font size': getColumn(features, 9),
        'output': labels
    })

def getColumn(arr2d, columnIndex):
    return [x[columnIndex] for x in arr2d]