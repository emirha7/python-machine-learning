from sklearn.dummy import DummyClassifier
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score

def train(x,y):
    print("========== dummy ================")
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.1, random_state=0)
    dummy_clf = DummyClassifier(strategy="most_frequent")
    dummy_clf.fit(X_train, y_train)
    print("dummy score = ",dummy_clf.score(X_test, y_test))

    scores = cross_val_score(dummy_clf, X_test, y_test, cv=10)
    print("mean = ", scores.mean(), " stdev = ", scores.std())