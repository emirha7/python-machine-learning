from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
import Analyzer as analyzer

def train(x,y):
    print("===================== SVM CLASSIFIER =====================")
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)
    clf = SVC() #slb
    #clf = SVC(kernel="rbf", gamma=0.00001, C=1000, probability=True)
    #clf = SVC(kernel="poly") sploh se ne zracuna
    #clf = SVC(kernel="sigmoid", gamma=0.00001, C=1000, probability=True) #slab
    #clf = SVC(kernel="linear")  # dobr, pocasn

    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    y_score = clf.fit(X_train, y_train).decision_function(X_test)
    analyzer.analyzeSVM(y_pred, y_test, y_score, clf, X_test)