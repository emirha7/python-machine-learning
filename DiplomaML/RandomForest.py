from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_score
import matplotlib.pyplot as plt

def train(x,y):
    print("===================== RANDOM FOREST =====================")
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)

    legendLabels = ()
    for i in range(1, 10):
        clf = RandomForestClassifier(n_estimators=100)
        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        y_pred_proba = clf.predict_proba(X_test)
        fpr, tpr, thr = roc_curve(y_test, y_pred_proba[:, 1])
        auc = roc_auc_score(y_test, y_pred_proba[:, 1])
        f1 = f1_score(y_test, y_pred)
        acc = accuracy_score(y_test, y_pred)

        print("TEST")
        #t = clf.predict_proba([[0, 86, 125, 1, 1, 43, 1, 0, 0, 1]])
        #pr = clf.predict([[0, 8, 0, 16, 2, 90, 1, 1, 0, 1]])

        aa = [[1, 106, 0, 17, 2, 76, 1, 1, 3, 1]]
        t = clf.predict_proba(aa)
        pr = clf.predict(aa)

        print(t)
        print("?????????????????")
        print(pr)
        print("-----------------------")
        scores = cross_val_score(clf, X_test, y_test, cv=10)
        print("mean = ", scores.mean(), " stdev = ", scores.std())
        print("AUC = ", auc, "accuracy = ", acc)
        print("F1 = ", f1)
        plt.plot(fpr, tpr, label="p" + str(i))
        plt.legend(str(i))
        legendLabels = legendLabels + ("p" + str(i),)

    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.legend(legendLabels)
    plt.show()
