userActivityHM = {
    "STILL": 0,
    "ON_FOOT": 1,
    "IN_VEHICLE": 2
}

internetHM = {
    "4G": 2,
    "3G": 1,
    "2G": 0
}

imageViewHM = {
    "withImages": 1,
    "noImages": 0,

}
themeViewHM = {
    "light-theme": 1,
    "dark-theme": 0
}

layoutViewHM = {
    "largeCards": 0,
    "gridView": 1,
    "miniCards": 2,
    "xLargeCards": 3
}

fontSizeViewHM = {
    "small-font": 0,
    "large-font": 1
}
