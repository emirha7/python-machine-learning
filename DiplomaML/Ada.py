from sklearn.ensemble import AdaBoostClassifier
from sklearn.model_selection import train_test_split
import Analyzer as analyzer

def train(x,y):
    print("===================== ADA BOOST CLASSIFIER =====================")
    clf = AdaBoostClassifier(n_estimators=100, learning_rate=1)
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)
    clf.fit(X_train, y_train)

    y_pred = clf.predict(X_test)
    y_pred_prob = clf.predict_proba(X_test)
    analyzer.drawGraph(y_test, y_pred, y_pred_prob, "gauss", clf, X_test)