import NodeObj
from csv import reader
from random import seed
from random import randrange
from math import sqrt


# Load a CSV file
def load_csv(filename):
    dataset = list()
    with open(filename, 'r') as file:
        csv_reader = reader(file)
        for row in csv_reader:
            if not row:
                continue
            dataset.append(row)
    return dataset


# Convert string column to float
def str_column_to_float(dataset, column):
    for row in dataset:
        row[column] = float(row[column].strip())


# Convert string column to integer
def str_column_to_int(dataset, column):
    class_values = [row[column] for row in dataset]
    unique = set(class_values)
    lookup = dict()
    for i, value in enumerate(unique):
        lookup[value] = i
    for row in dataset:
        row[column] = lookup[row[column]]
    return lookup


# Split a dataset into k folds
def cross_validation_split(dataset, n_folds):
    dataset_split = list()
    dataset_copy = list(dataset)
    fold_size = int(len(dataset) / n_folds)
    for i in range(n_folds):
        fold = list()
        while len(fold) < fold_size:
            index = randrange(len(dataset_copy))
            fold.append(dataset_copy.pop(index))
        dataset_split.append(fold)
    return dataset_split


# Calculate accuracy percentage
def accuracy_metric(actual, predicted):
    correct = 0
    for i in range(len(actual)):
        if actual[i] == predicted[i]:
            correct += 1
    return correct / float(len(actual)) * 100.0


# Evaluate an algorithm using a cross validation split
def evaluate_algorithm(dataset, algorithm, n_folds, *args):
    folds = cross_validation_split(dataset, n_folds)
    scores = list()
    for fold in folds:
        train_set = list(folds)
        train_set.remove(fold)
        train_set = sum(train_set, [])
        test_set = list()
        for row in fold:
            row_copy = list(row)
            test_set.append(row_copy)
            row_copy[-1] = None
        predicted = algorithm(train_set, test_set, *args)
        actual = [row[-1] for row in fold]
        accuracy = accuracy_metric(actual, predicted)
        scores.append(accuracy)
    return scores


def gini_index(groups, classes):
    n = float(sum([len(group) for group in groups]))
    gini = 0

    for group in groups:
        g_size = float(len(group))
        if g_size == 0:
            continue

        p_sum = 0
        for class_val in classes:
            c = float([row[-1] for row in group].count(class_val))
            p_sum += (c / g_size) ** 2
        gini += (g_size / n) * (1 - p_sum)

    return gini


def test_split(index, value, dataset):
    left, right = list(), list()
    for row in dataset:
        if row[index] < value:
            left.append(row)
        else:
            right.append(row)
    return left, right


def get_split(dataset, n_features):
    classes = list(set(row[-1] for row in dataset))
    b_index, b_value, b_score, b_groups = 999, 999, 999, None
    features = list()

    for _ in range(n_features):
        index = randrange(len(dataset[0]) - 1)
        if index not in features:
            features.append(index)

    for index in features:
        for row in dataset:
            groups = test_split(index, row[index], dataset)
            gini = gini_index(groups, classes)
            if gini < b_score:
                b_index, b_value, b_score, b_groups = index, row[index], gini, groups

    return {'index': b_index, 'value': b_value, 'groups': b_groups}


def predict(node, row):
    if node.is_terminal:
        return node.decide()

    index = node.index
    if row[index] < node.value:
        return predict(node.children[0], row)
    else:
        # mlahav selotejp fix, ker ce je podatke razbiu na 2 dela in je en prazn, lah da ma sam en node,
        # za tega pa neves a je levo al je desno, ker tega nikjer ne belezis.
        if len(node.children) == 1:
            return predict(node.children[0], row)
        return predict(node.children[1], row)


def subsample(dataset, ratio):
    sample = list()
    n_sample = round(len(dataset) * ratio)
    while len(sample) < n_sample:
        index = randrange(len(dataset))
        sample.append(dataset[index])
    return sample


def bagging_predict(trees, row):
    predictions = [predict(tree, row) for tree in trees]
    return max(set(predictions), key=predictions.count)


# build_tree(sample, max_depth, min_size, min_size, n_features)
def build_tree(dataset, max_depth, min_nodes, current_depth, n_features):
    data_split = get_split(dataset, n_features)
    groups = data_split['groups']
    value = data_split['value']
    index = data_split['index']
    root = NodeObj.Node(value, index)

    if current_depth >= max_depth or len(dataset) <= min_nodes:
        for group in groups:
            if len(group) == 0:
                continue
            node = NodeObj.Node(-1, -1)
            node.make_it_terminal()
            node.rows_array = group.copy()

            root.children.append(node)
        return root

    for group in groups:
        if len(group) == 0:
            continue
        n = build_tree(group, max_depth, min_nodes, current_depth + 1, n_features)
        root.children.append(n)

    return root


def random_forest(train, test, max_depth, min_size, sample_size, n_trees, n_features):
    trees = list()
    for _ in range(n_trees):
        sample = subsample(train, sample_size)
        trees.append(build_tree(sample, max_depth, min_size, 1, n_features))
    predictions = [bagging_predict(trees, row) for row in test]
    return (predictions)


seed(2)
filename = 'sonar.all-data.csv'
dataset = load_csv(filename)

for i in range(0, len(dataset[0]) - 1):
    str_column_to_float(dataset, i)

str_column_to_int(dataset, len(dataset[0]) - 1)

n_folds = 5
max_depth = 10
min_size = 1
sample_size = 1.0
n_features = int(sqrt(len(dataset[0]) - 1))

for n_trees in [100]:
    scores = evaluate_algorithm(dataset, random_forest, n_folds, max_depth, min_size, sample_size, n_trees, n_features)
    print('Trees: %d' % n_trees)
    print('Scores: %s' % scores)
    print('Mean Accuracy: %.3f%%' % (sum(scores) / float(len(scores))))
