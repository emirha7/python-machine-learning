class Node:
    def __init__(self, value, index):
        self.value = value
        self.children = []
        self.rows_array = []
        self.is_terminal = False
        self.prediction_class = None
        self.index = index

    def make_it_terminal(self):
        self.is_terminal = True

    def decide(self):
        a = [row[-1] for row in self.rows_array]
        return max(set(a), key=a.count)
