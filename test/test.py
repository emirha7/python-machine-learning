# thompson.py
# Python 3.6.5

import numpy as np
import random
import math

def main():
  print("Begin Thompson sampling demo ")
  print("Goal is to maximize payout from three machines")
  print("Machines pay out with probs 0.3, 0.7, 0.5")

  N = 5  # number machines
  means = [[10/100, 15/100, 12/100, 22/100, 32/100, 9/100],
           [10 / 100, 15 / 100, 12 / 100, 22 / 100, 32 / 100, 9 / 100],
           [10 / 100, 15 / 100, 12 / 100, 22 / 100, 32 / 100, 9 / 100],
           [10/100, 7/100, 14/100, 20/100, 29/100, 20/100],
           [25/100, 10/100, 12/100, 2/100, 42/100, 9/100]]
  means_ex = [[0]]*N
  #means = np.array([0.3, 0.7, 0.5])
  probs = np.zeros(N)
  #S = np.zeros(N, dtype=np.int)
  #F = np.zeros(N, dtype=np.int)
  #trials = [0,0,0,0,0]
  results = [0,0,0,0,0]
  #rnd = np.random.RandomState(7)
  #print("a = ",np.random.normal(6,6))
  machines_selected = []
  sum_of_reward = [0]*N
  number_of_selections = [0]*N
  total_reward = 0
  for trial in range(100):
    machine = 0
    max_upper_bound = 0
    for i in range(0, N):
        if number_of_selections[i] > 0:
            average_reward = sum_of_reward[i] / number_of_selections[i]
            delta_i = math.sqrt(2 * math.log(trial + 1) / number_of_selections[i])
            upper_bound = average_reward + delta_i
        else:
            upper_bound = 1e400
        if upper_bound > max_upper_bound:
            max_upper_bound = upper_bound
            machine = i
    machines_selected.append(machine)
    number_of_selections[machine] += 1
    reward = generateNext(means[machine]) / 18

    sum_of_reward[machine] += reward
    total_reward += reward

  print(machines_selected)
  print(number_of_selections)
  print(total_reward)

  """
    for i in range(N):
      #probs[i] = rnd.beta(S[i] + 1, F[i] + 1)
      probs[i] = np.random.beta(S[i] + 1, F[i] + 1)
      print("generatedVal = ",probs[i])

    print("sampling probs =  ", end="")
    for i in range(N):
      print("%0.4f  " % probs[i], end="")
    print("")

    machine = np.argmax(probs)
    print("Playing machine " + str(machine), end="")
    trials[machine]+=1

    #p = rnd.random_sample()  # [0.0, 1.0)
    reward = generateNext(means[machine])
    if reward > 3:
        S[machine]+=1
    else:
        F[machine]+=1

    #S[machine] += reward
    #F[machine] += (6 - reward)
    #results[machine]+=reward
    """

    #if p < means[machine]:
    #  print(" -- win")
    #  S[machine] += 1
    #else:
    #  print(" -- lose")
    #  F[machine] += 1"""
  """
  print("Final Success vector: ", end="")
  print(S)
  print("Final Failure vector: ", end="")
  print(F)
  print("TRIALS = ",trials)
  #print([S[i]/trials[i] for i in range(0, len(trials))])"""


def generateNext(probs):
    rndVal = random.uniform(0, 1)
    lowerbound = 0
    for index in range(0, len(probs)):
        val = probs[index]
        if(lowerbound < rndVal and rndVal <= lowerbound + val):
            return (index + 1)*3
        else:
            lowerbound += val

if __name__ == "__main__":
  main()